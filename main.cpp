/*
 *  Copyright 2018 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "grabwindow.h"

#include <QApplication>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickItem>
#include <QQuickRenderControl>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("QML grabber");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("source", QCoreApplication::translate("main", "QML file to display."));


    QCommandLineOption secondsOption(QStringList() << "s" << "seconds",
                                     QCoreApplication::translate("main", "Record for <seconds>, if 0(default) will save a single frame."),
                                     QCoreApplication::translate("main", "seconds"));
    secondsOption.setDefaultValue(QChar('0'));
    parser.addOption(secondsOption);

    QCommandLineOption thirdOption(QStringList() << "f" << "fps",
                                     QCoreApplication::translate("main", "Record with <fps>, default is 60."),
                                     QCoreApplication::translate("main", "fps"));
    thirdOption.setDefaultValue("60");
    parser.addOption(thirdOption);

    QCommandLineOption forthOption(QStringList() << "a" << "autostart",
                                     QCoreApplication::translate("main", "Autostart recording."),
                                     QCoreApplication::translate("main", "0 or 1"));
    forthOption.setDefaultValue("1");
    parser.addOption(forthOption);

    parser.process(app);

    const QStringList args = parser.positionalArguments();
    if (args.length() != 1) {
        parser.showHelp(0);
    }

    quint32 seconds = parser.value(secondsOption).toUInt();
    quint32 fps = parser.value(thirdOption).toUInt();
    bool autostart = parser.value(forthOption).toInt() == 1;
    ScalableQuickRenderControl *renderControl = new ScalableQuickRenderControl();
    GrabWindow view(args.first(), seconds, renderControl, fps, autostart);

    return app.exec();
}
