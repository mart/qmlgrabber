/*
 *  Copyright 2018 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "grabwindow.h"
#include "animationdriver.h"

#include <iostream>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickItem>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickRenderControl>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>
#include <QTimer>
#include <QScreen>
#include <QDir>

ScalableQuickRenderControl::ScalableQuickRenderControl(QObject *parent)
    : QQuickRenderControl(parent)
{}

QWindow *ScalableQuickRenderControl::renderWindow(QPoint *offset)
{
    return m_window;
}

GrabWindow::GrabWindow(const QString &source, quint32 seconds, ScalableQuickRenderControl *control, quint32 fps, bool autostart)
    : QQuickWindow(control),
      m_renderControl(control),
      m_engine(new QQmlEngine(this))
{
    control->m_window = this;
    m_fps = fps;
    m_totalFrames = qMax((quint32)1, seconds * m_fps);

    m_animationDriver = new AnimationDriver(1000 / m_fps);
    m_animationDriver->install();

    setClearBeforeRendering(true);

    QSurfaceFormat format;
    format.setDepthBufferSize(16);
    format.setStencilBufferSize(8);

    m_context = new QOpenGLContext(this);
    m_context->setFormat(format);
    m_context->create();

    m_offscreenSurface = new QOffscreenSurface;
    m_offscreenSurface->setFormat(m_context->format());
    m_offscreenSurface->create();

    if (!m_engine->incubationController()) {
         m_engine->setIncubationController(incubationController());
    }
    QQmlComponent component(m_engine, source);
    if (component.isError()) {
        const QList<QQmlError> errorList = component.errors();
        foreach (const QQmlError &error, errorList) {
            qWarning() << error.url() << error.line() << error;
        }

        qFatal("Unable to load QML file");
    }
    // allow start/stop of recording from inside the qml
    qmlcontroler *ctrl = new qmlcontroler();
    ctrl->m_window = this;
    m_engine->rootContext()->setContextProperty("qmlControler", ctrl);

    // Create Frames subdir if we record a video
    if (m_totalFrames > 1) {
        if (!QDir::current().mkdir("Frames")) {
            qFatal("Unable to create the Frames directory");
        }
    }

    QObject *obj = component.create();
    if ((this->m_root = qobject_cast<QQuickItem *>(obj))) {
        this->m_root->setParentItem(contentItem());
        resize(this->m_root->size().toSize());
        ctrl->m_window = this;
    }

    m_context->makeCurrent(m_offscreenSurface);
    m_renderControl->initialize(m_context);

    m_fbo = new QOpenGLFramebufferObject(size() * screen()->devicePixelRatio(), QOpenGLFramebufferObject::CombinedDepthStencil);
    setRenderTarget(m_fbo);

    if (autostart) {
        m_running = true;
        renderFrame();
    }
    if (m_totalFrames > 1) {
        std::cout << "Recording " << seconds << "seconds, dumping " << m_totalFrames << " frames at 60 fps\n";
    } else {
        std::cout << "Saving Screenshot.png\n";
    }
}

GrabWindow::~GrabWindow()
{
    delete m_fbo;
    m_context->doneCurrent();
}

void GrabWindow::setRunning(bool running)
{
    m_running = running;
    if (running) {
        renderFrame();
    }
}

bool GrabWindow::event(QEvent *event)
{
    if (event->type() == QEvent::UpdateRequest && m_running) {
        renderFrame();
        return true;
    }

    return QQuickWindow::event(event);
}

void GrabWindow::renderFrame()
{

    m_context->makeCurrent(m_offscreenSurface);
    m_renderControl->polishItems();
    m_renderControl->sync();
    m_renderControl->render();
    m_context->functions()->glFlush();

    ++m_frameCounter;
    frames.append(m_renderControl->grab());
    // If a popup overlay exists, it needs to be manually sized
    QQuickItem *overlay = property("_q_QQuickOverlay").value<QQuickItem *>();
    if (overlay) {
        overlay->setWidth(width());
        overlay->setHeight(height());
    }

    m_animationDriver->advance();

    
    if (m_frameCounter < m_totalFrames) {
        //Schedule the next update
        QEvent *updateRequest = new QEvent(QEvent::UpdateRequest);
        QCoreApplication::postEvent(this, updateRequest);

    } else {
        this->writeFrames();
        std::cout << "Frame dump completed.\n";
        qApp->quit();
    }
}

void GrabWindow::writeFrames()
{
    if (frames.size() == 1 ){
        // Saving just a singkle screenshot
        frames.at(0).save("Screenshot.png");
    }
    else {
        //S aving for video
        for(int i=0;i<frames.size();++i){
              frames.at(i).save(QStringLiteral("Frames/Frame-%1.png").arg(i));
         }
    }
}
