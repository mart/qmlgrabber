#ifndef QMLCONTROLER_H
#define QMLCONTROLER_H

#include <QObject>
#include <QQmlEngine>
#include <QQuickItem>
#include <QQmlComponent>
#include <QQuickView>
#include <QQuickWindow>
#include <QOffscreenSurface>
#include <QTimer>

class GrabWindow;

class qmlcontroler : public QObject
{
    Q_OBJECT
public:
    explicit qmlcontroler(QObject *parent = nullptr);
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    GrabWindow *m_window;

signals:

public slots:
};

#endif // QMLCONTROLER_H
