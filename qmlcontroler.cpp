#include "qmlcontroler.h"
#include "grabwindow.h"
#include <iostream>
#include <QGraphicsSceneMouseEvent>
#include <QMouseEvent>
#include <QApplication>
#include <QTimer>
#include <QtMath>

qmlcontroler::qmlcontroler(QObject *parent) : QObject(parent)
{

}

void qmlcontroler::start()
{
    m_window->setRunning(true);
}

void qmlcontroler::stop()
{
    m_window->setRunning(false);
}
