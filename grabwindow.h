/*
 *  Copyright 2018 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GRABWINDOW_H
#define GRABWINDOW_H

#include <QQuickWindow>
#include <QQuickRenderControl>
#include "qmlcontroler.h"

class QQuickRenderControl;
class QOffscreenSurface;
class QOpenGLContext;
class QOpenGLFramebufferObject;
class QTimer;

class AnimationDriver;

class ScalableQuickRenderControl : public QQuickRenderControl
{
public:
    ScalableQuickRenderControl(QObject *parent = nullptr);
    QWindow *renderWindow(QPoint *offset) override;
    QWindow *m_window = nullptr;
};

class GrabWindow : public QQuickWindow
{
public:
    GrabWindow(const QString &source, quint32 seconds, ScalableQuickRenderControl *control, quint32 fps, bool autostart);
    ~GrabWindow();

    void renderFrame();
    void writeFrames();
    void setRunning(bool running);

protected:
    bool event(QEvent *event) override;

private:
    QTimer *m_timer;
    QOpenGLFramebufferObject *m_fbo;
    QOpenGLContext *m_context;
    QOffscreenSurface *m_offscreenSurface;
    QQuickRenderControl *m_renderControl;
    AnimationDriver *m_animationDriver;
    QQmlEngine *m_engine;
    quint32 m_frameCounter = 0;
    quint32 m_fps = 60;
    quint32 m_totalFrames = 0;
    quint32 m_seconds = 0;
    QList<QImage> frames;
    qmlcontroler m_qmlcontroler;

    QQuickItem *m_root;
    bool m_running = false;
};

#endif // GRABWINDOW_H
