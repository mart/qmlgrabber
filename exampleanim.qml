

import QtQuick 2.9
import QtQuick.Controls 2.2

Grid {
    columns: 4
    Button {
        text: "Button"
    }
    Repeater {
        model: 16
        Item {
            width: 110
            height: 110
            Rectangle {
                id: rect
                antialiasing: true
                color: "red"
                width: 100
                height: 100
                anchors.centerIn: parent
            }
            RotationAnimator {
                target: rect
                loops: Animation.Infinite
                from: 0;
                to: 360;
                duration: 1000 * (modelData+1)
                running: true
            }
        }
    }
}
